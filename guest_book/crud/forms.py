from django.core import validators
from django import forms
from .models import guest_book_models


class guest_book_forms(forms.ModelForm):
    class Meta:
        model = guest_book_models
        fields = ['nama', 'email', 'alamat']
        widgets = {
            'nama': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'alamat': forms.Textarea(attrs={'class': 'form-control'})
        }
