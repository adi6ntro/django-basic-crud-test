from django.shortcuts import render, HttpResponseRedirect
from .forms import guest_book_forms
from .models import guest_book_models
# Create your views here.


def add_guest(request):
    if request.method == 'POST':
        fm = guest_book_forms(request.POST)
        if fm.is_valid():
            nm = fm.cleaned_data['nama']
            em = fm.cleaned_data['email']
            al = fm.cleaned_data['alamat']
            reg = guest_book_models(nama=nm, email=em, alamat=al)
            reg.save()
            fm = guest_book_forms()
    else:
        fm = guest_book_forms()
    guest = guest_book_models.objects.all()
    return render(request, "crud/add.html", {'form': fm, 'guest': guest})


def update_guest(request, id):
    if request.method == 'POST':
        pi = guest_book_models.objects.get(pk=id)
        fm = guest_book_forms(request.POST, instance=pi)
        if fm.is_valid():
            fm.save()
            return HttpResponseRedirect('/')
    else:
        pi = guest_book_models.objects.get(pk=id)
        fm = guest_book_forms(instance=pi)
    return render(request, "crud/update.html", {'form': fm})


def delete_guest(request, id):
    if request.method == 'POST':
        pi = guest_book_models.objects.get(pk=id)
        pi.delete()
        return HttpResponseRedirect('/')
