from django.contrib import admin

# Register your models here.
# from crud.models import crud_guest_book
# admin.site.register(crud_guest_book)

from .models import guest_book_models


@admin.register(guest_book_models)
class guest_book_admin(admin.ModelAdmin):
    list_display = ('id', 'nama', 'email', 'alamat')
