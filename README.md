# django-basic-crud-test

For Assessment

## Installation

Important to install Python version >= 3.7
Create a Python 3 virtual environment

```bash
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ _
```

Then, install every necessary python module

```bash
$ pip install -r requirements.txt
```

## Usage

Run your apps:

```bash
(venv) $ cd guest_book
(venv) $ python3 manage.py makemigrations
(venv) $ python3 manage.py migrate
(venv) $ python3 manage.py runserver
```

## On Browser

[http://localhost:8000/](http://localhost:8000/) - as Front End.
[http://localhost:8000/admin](http://localhost:8000/admin) - as Back End.
**Username: admin, Password: admin**